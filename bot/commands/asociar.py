from telegram.ext import CommandHandler

from bot.db import Vars
from bot.utils import restricted

COMMAND_NAME = 'asociar'

def create_handler():
    return CommandHandler(COMMAND_NAME, command)

@restricted
def command(update, context):
    bot_user = context.bot.bot
    if not bot_user.can_read_all_group_messages:
        update.message.reply_text(
            '✴️ Debo estar configurado para leer los mensajes!'\
            ' Hacelo, re-agregame y volvé a intentar.')
    else:
        chat_id = update.effective_chat.id

        Vars.set_admin_group(chat_id)

        update.message.reply_text(f'✅ Chat actual asociado como grupo admin')
