import importlib

all_command_names = [
    'start',
    'debug',
    'admin_admit',
    'admin_reject',
    'asociar',
]

all_command_handlers = []

for command_name in all_command_names:
    command_module = importlib.import_module(f'.{command_name}', package='bot.commands')
    all_command_handlers.append(command_module.create_handler())
