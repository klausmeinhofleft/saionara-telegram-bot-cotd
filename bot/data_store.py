import collections

# namedtuple - https://www.tutorialspoint.com/namedtuple-in-python
MensajePendiente = collections.namedtuple('MensajePendiente', ['chat_id', 'msg'])

class MensajesPendientes:
    def has_mine(update, context):
        if not 'mensajes_pendientes' in context.bot_data:
            return False
        username = update.effective_user.username
        return username in context.bot_data['mensajes_pendientes']

    def add_mine(update, context, msg):
        chat_id = update.effective_chat.id
        username = update.effective_user.username

        if not 'mensajes_pendientes' in context.bot_data:
            context.bot_data['mensajes_pendientes'] = {}

        mensajes_pendientes = context.bot_data['mensajes_pendientes']

        mensaje_pendiente_data = {
            'chat_id': chat_id,
            'msg': msg
        }

        mensajes_pendientes[username] = mensaje_pendiente_data
        print(f'Mensaje pendiente agregado para usuarix={username} chat_id={chat_id}')

        return MensajePendiente(**mensaje_pendiente_data)

    def get_user(context, username):
        mensaje_pendiente_data = context.bot_data['mensajes_pendientes'][username]
        return MensajePendiente(**mensaje_pendiente_data)

    def remove_user(context, username):
        del context.bot_data['mensajes_pendientes'][username]
        print(f'Mensaje pendiente eliminado para usuarix={username}')
