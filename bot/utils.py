from functools import wraps
import re

from bot.db import Admin

def restricted(func):
    @wraps(func)
    def wrapped(update, context, *args, **kwargs):
        username = update.effective_user.username
        # https://docs.peewee-orm.com/en/latest/peewee/quickstart.html#lists-of-records
        is_admin = Admin.has_user(username)
        if not is_admin:
            print(f'Operación negada para usuarie no admin @{username}')
            update.message.reply_text('🔐 Solo les admins pueden hacer eso!')
            return
        return func(update, context, *args, **kwargs)
    return wrapped

def escape_markdown_entities(text):
    return text \
        .replace('\\', '\\\\') \
        .replace('.', '\.') \
        .replace('(', '\(') \
        .replace(')', '\)') \
        .replace('_', '\_') \
        .replace('*', '\*')

def remove_markdown_entities(markdown):
    # no me funcionaba en non-capture group (?:) así que lo partí en dos
    sub1 = re.sub(r'(__|[*~_`])([^ ])', r'\2', markdown)
    sub2 = re.sub(r'([^ ])(__|[*~_`])', r'\1', sub1)
    return sub2
