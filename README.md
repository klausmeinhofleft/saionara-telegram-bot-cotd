__Bot de Telegram que permite a usuarixs anónimxs mandar posteos a un canal a nombre del bot, con moderación de admins__

Viene con un watch-er de cambios de código para recargar el bot automáticamente

## Funcionamiento

La idea es que cualquier persona le pueda hablar al bot por privado y hacer `/start`
para proponer un posteo. Ese posteo irá a parar al grupo admin donde lxs admins
podrán aceptar o rechazar el posteo con unos botones que aparecerán. Al aceptarlo,
automáticamente se manda el mensaje al canal, a nombre del bot. Al rechazarlo, el bot ofrece que le
escribas el motivo del rechazo (opcional). En ambos casos el bot le avisa a lx usuarix original
lo que sucedió con su posteo.

Los comandos (en formato para pasarle a [@BotFather](https://telegram.me/botfather)) son:
> start - [público] Comienza proceso de posteo  
> asociar - [admins] Asocia el bot al grupo actual (grupo admin)   
> debug - [admins] Devuelve datos de debugeo   
> cancel - Cancela cualquier comando en transcurso   

## Instalación

1. Copiar `ejemplo.env` a `.env` y cambiar variables acorde a tu caso
1. Instalar requerimientos de python haciendo `source install-requirements.sh`
1. Activar el venv si no estaba activado `source venv/bin/activate`
1. Ejecutar `python3 bot.py`, o `python3 watcher.py` si lo desean correr con
en modo recarga automática de cambios

Para usos posteriores solo hará falta ejecutar a partir del paso 3

## Configuración

### Dentro de Telegram
Van a necesitar configurar el bot para que se pueda unir a grupos y que pueda leer mensajes
de los grupos (desde [@BotFather](https://telegram.me/botfather)). Van a requerir un grupo 
privado de admins para que reciba los posteos propuestos y un canal donde irán a parar los posteos. 
El canal se setea desde las variables de entorno y el grupo admin con el comando `/asociar` enviado 
al bot desde adentro del grupo admin (con el bot agregado como miembro).

### Variables de entorno
Variables del `.env` y `docker-compose.yml`:
- `DB_FILE` es el nombre del archivo que se le dará a la base de datos Sqlite3
- `LOGGING` activar los logs del bot: 1 o yes para sí, 0 o no para no
- `LOGGING_SQL` lo mismo que lo anterior pero para los comando SQL ejecutados
- `BOT_TOKEN` es el token del bot, en el formato NÚMERO:LETRAS_Y_SÍMBOLOS
- `INITIAL_ADMINS` listado de usernames de admins separados por comas
- `CHANNEL` username del canal público al que los posteos irán a parar

Si en algún momento desean reiniciar la base de datos simplemente borrar su archivo.

## Docker

Para correr en docker primero build-ear la imagen:

`docker build -t telegram-bot-cotd .`

Y después correrla pasándole los parámetros correspondientes:

`docker run --rm --env-file=.env telegram-bot-cotd`

### Persistencia

Para hacerle la base de datos persistente es medio quilombo.
Para no poluir tu instalación de docker se puede agregar el siguiente parámetro
al principio del comando anterior (después del `run`): `-v "$(pwd)/docker_data:/code/db"`

Además hay que agregarle el directorio `db/` al parámetro `DB_FILE`, pasando de `DB_FILE=labase.db` a `DB_FILE=db/labase.db`.

La intrucción completa quedaría:

`docker run -v "$(pwd)/docker_data:/code/db" --env-file=.env telegram-bot-cotd`
